import moment from "moment"
import Brands from "../models/brands"

const population = [
  {
    path: "created_by",
    select: "-password",
  },
]

class BrandDal {
  constructor() {}

  static get(query, cb) {
    Brands.findOne(query)
      .populate(population)
      .exec((err, doc) => {
        if (err) {
          return cb(err)
        }
        cb(null, doc || {})
      })
  }

  static create(BrandsData, cb) {
    const BrandsModel = new Brands(BrandsData)

    BrandsModel.save(function saveBrands(err, data) {
      if (err) {
        return cb(err)
      }

      BrandDal.get({ _id: data._id }, (err, doc) => {
        if (err) {
          return cb(err)
        }

        cb(null, doc)
      })
    })
  }

  static delete(query, cb) {
    Brands.findOne(query)
      .populate(population)
      .exec(function deleteBrands(err, doc) {
        if (err) {
          return cb(err)
        }

        if (!doc) {
          return cb(null, {})
        }

        Brands.remove(query, err => {
          if (err) {
            return cb(err)
          }

          cb(null, doc)
        })
      })
  }

  static update(query, updates, cb) {
    const now = moment().toISOString()
    const opts = {
      new: true,
    }

    Brands.findOneAndUpdate(query, updates, opts)
      .populate(population)
      .exec(function updateBrands(err, doc) {
        if (err) {
          return cb(err)
        }
        cb(null, doc || {})
      })
  }

  static getCollection(query, opt, cb) {
    Brands.find(query, {}, opt)
      .sort({ title: 1 })
      .populate(population)
      .exec(function getBrandssCollection(err, doc) {
        if (err) {
          return cb(err)
        }
        return cb(null, doc)
      })
  }
}

export default BrandDal
