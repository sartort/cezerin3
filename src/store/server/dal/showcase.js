import ShowCase from "../models/brandsShowCase"

const population = [
  {
    path: "created_by",
    select: "-password",
  },
]

class ShowCaseDal {
  constructor() {}

  static get(query, cb) {
    ShowCase.findOne(query)
      .populate(population)
      .exec((err, doc) => {
        if (err) {
          return cb(err)
        }

        cb(null, doc || {})
      })
  }

  static create(ShowCaseData, cb) {
    const ShowCaseModel = new ShowCase(ShowCaseData)

    ShowCaseModel.save(function saveShowCase(err, data) {
      if (err) {
        return cb(err)
      }

      ShowCaseDal.get({ _id: data._id }, (err, doc) => {
        if (err) {
          return cb(err)
        }

        cb(null, doc)
      })
    })
  }

  static delete(query, cb) {
    ShowCase.findOne(query)
      .populate(population)
      .exec(function deleteShowCase(err, doc) {
        if (err) {
          return cb(err)
        }
        if (!doc) {
          return cb(null, {})
        }
        ShowCase.remove(query, err => {
          if (err) {
            return cb(err)
          }

          cb(null, doc)
        })
      })
  }

  static update(query, updates, cb) {
    const opts = {
      new: true,
    }

    ShowCase.findOneAndUpdate(query, updates, opts)
      .populate(population)
      .exec(function updateShowCase(err, doc) {
        if (err) {
          return cb(err)
        }

        cb(null, doc || {})
      })
  }

  static getCollection(query, opt, cb) {
    ShowCase.find(query, {}, opt)
      .populate(population)
      .exec(function getShowCasesCollection(err, doc) {
        if (err) {
          return cb(err)
        }
        return cb(null, doc)
      })
  }
}

export default ShowCaseDal
